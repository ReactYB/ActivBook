import { useEffect } from "react";
import { Link } from "react-router-dom";


const HomePage = () => {

  useEffect(() => {

  }, []);
  return (
    <div id="homePage" className="h-screen text-center" >
      <div
        id="textHomePage"
        className="flex flex-col justify-center h-full justify-items-center"
        style={{ padding: "1rem" }}
      >
        <div
          className=""
          id="transform"

        >
          Home Page
        </div>
        <div >
          Go to <Link to={`/activities`}>Main Page</Link>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
