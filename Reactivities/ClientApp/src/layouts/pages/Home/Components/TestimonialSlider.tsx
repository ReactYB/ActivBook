import Slider from "react-slick";
import YBArrowLeft from "../../../../components/YBSlick/YBArrowLeft";
import YBArrowRight from "../../../../components/YBSlick/YBArrowRight";

const TestimonialSlider = () => {
 
  const testimonialSetting = {
    infinite: false,
    dots: true,
    slidesToShow: 3,
    accessibility: true,
    slidesToScroll: 1,
    centerMode:false,
    rtl:false,
    cssEase: "cubic-bezier(0.7, 0, 0.3, 1)",
    nextArrow: <YBArrowRight />,
    prevArrow: <YBArrowLeft />,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <section className="relative">
      <div className="absolute inset-x-0 top-0 flex justify-center pt-16 pb-8 pointer-events-none bg-gradient-to-b from-white/60 dark:from-slate-900"></div>
      <div className="px-5 mx-auto max-w-7xl py-36">
        <Slider {...testimonialSetting}>
          <div className="px-2 py-4 bg-gray-300 shadow-lg lg:mb-0 h-max rounded-xl dark:bg-gray-700 dark:text-gray-400">
            <div className="w-full h-full text-center">
              <img
                alt="testimonial"
                className="inline-block object-cover object-center w-20 h-20 mx-auto mb-8 rounded-full"
                src="https://picsum.photos/200"
              />
              <p className="text-slate-900 dark:text-slate-300">
                Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's
                cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag
                drinking vinegar cronut adaptogen squid fanny pack vaporware.
              </p>
              <span className="inline-block w-10 h-1 mt-6 mb-4 rounded bg-primary dark:bg-primaryDark"></span>
              <h2 className="font-bold tracking-wider text-gray-900 dark:text-slate-400 title-font">
                HOLDEN CAULFIELD
              </h2>
              <p className="">Senior Product Designer</p>
            </div>
          </div>
          <div className="px-2 py-4 bg-gray-300 shadow-lg lg:mb-0 h-max rounded-xl dark:bg-gray-700 dark:text-gray-400">
            <div className="w-full h-full text-center">
              <img
                alt="testimonial"
                className="inline-block object-cover object-center w-20 h-20 mx-auto mb-8 rounded-full"
                src="https://picsum.photos/200"
              />
              <p className="text-slate-900 dark:text-slate-300">
                Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's
                cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag
                drinking vinegar cronut adaptogen squid fanny pack vaporware.
              </p>
              <span className="inline-block w-10 h-1 mt-6 mb-4 rounded bg-primary dark:bg-primaryDark"></span>
              <h2 className="font-bold tracking-wider text-gray-900 dark:text-slate-400 title-font">
                HOLDEN CAULFIELD
              </h2>
              <p className="">Senior Product Designer</p>
            </div>
          </div>
          <div className="px-2 py-4 bg-gray-300 shadow-lg lg:mb-0 h-max rounded-xl dark:bg-gray-700 dark:text-gray-400">
            <div className="w-full h-full text-center">
              <img
                alt="testimonial"
                className="inline-block object-cover object-center w-20 h-20 mx-auto mb-8 rounded-full"
                src="https://picsum.photos/200"
              />
              <p className="text-slate-900 dark:text-slate-300">
                Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's
                cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag
                drinking vinegar cronut adaptogen squid fanny pack vaporware.
              </p>
              <span className="inline-block w-10 h-1 mt-6 mb-4 rounded bg-primary dark:bg-primaryDark"></span>
              <h2 className="font-bold tracking-wider text-gray-900 dark:text-slate-400 title-font">
                HOLDEN CAULFIELD
              </h2>
              <p className="">Senior Product Designer</p>
            </div>
          </div>
          <div className="px-2 py-4 bg-gray-300 shadow-lg lg:mb-0 h-max rounded-xl dark:bg-gray-700 dark:text-gray-400">
            <div className="w-full h-full text-center">
              <img
                alt="testimonial"
                className="inline-block object-cover object-center w-20 h-20 mx-auto mb-8 rounded-full"
                src="https://picsum.photos/200"
              />
              <p className="text-slate-900 dark:text-slate-300">
                Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's
                cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag
                drinking vinegar cronut adaptogen squid fanny pack vaporware.
              </p>
              <span className="inline-block w-10 h-1 mt-6 mb-4 rounded bg-primary dark:bg-primaryDark"></span>
              <h2 className="font-bold tracking-wider text-gray-900 dark:text-slate-400 title-font">
                HOLDEN CAULFIELD
              </h2>
              <p className="">Senior Product Designer</p>
            </div>
          </div>
          <div className="px-2 py-4 bg-gray-300 shadow-lg lg:mb-0 h-max rounded-xl dark:bg-gray-700 dark:text-gray-400">
            <div className="w-full h-full text-center">
              <img
                alt="testimonial"
                className="inline-block object-cover object-center w-20 h-20 mx-auto mb-8 rounded-full"
                src="https://picsum.photos/200"
              />
              <p className="text-slate-900 dark:text-slate-300">
                Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's
                cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag
                drinking vinegar cronut adaptogen squid fanny pack vaporware.
              </p>
              <span className="inline-block w-10 h-1 mt-6 mb-4 rounded bg-primary dark:bg-primaryDark"></span>
              <h2 className="font-bold tracking-wider text-gray-900 dark:text-slate-400 title-font">
                HOLDEN CAULFIELD
              </h2>
              <p className="">Senior Product Designer</p>
            </div>
          </div>
          <div className="px-2 py-4 bg-gray-300 shadow-lg lg:mb-0 h-max rounded-xl dark:bg-gray-700 dark:text-gray-400">
            <div className="w-full h-full text-center">
              <img
                alt="testimonial"
                className="inline-block object-cover object-center w-20 h-20 mx-auto mb-8 rounded-full"
                src="https://picsum.photos/200"
              />
              <p className="text-slate-900 dark:text-slate-300">
                Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's
                cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag
                drinking vinegar cronut adaptogen squid fanny pack vaporware.
              </p>
              <span className="inline-block w-10 h-1 mt-6 mb-4 rounded bg-primary dark:bg-primaryDark"></span>
              <h2 className="font-bold tracking-wider text-gray-900 dark:text-slate-400 title-font">
                HOLDEN CAULFIELD
              </h2>
              <p className="">Senior Product Designer</p>
            </div>
          </div>
        </Slider>
      </div>
    </section>
  );
};

export default  TestimonialSlider ;
