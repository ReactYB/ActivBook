import { useTranslation } from "react-i18next";

const HeroSection = () => {
  const { t } = useTranslation();
  return (
    <div className="relative max-w-5xl pt-20 mx-auto my-4 sm:pt-24 lg:pt-32">
      <h1 className="text-4xl font-extrabold tracking-tight text-center text-slate-900 sm:text-5xl lg:text-6xl dark:text-white">
        {t("sectionOne.title")}{" "}
      </h1>
      <p className="max-w-3xl mx-auto mt-6 text-lg text-center text-slate-600 dark:text-slate-400">
        {t("sectionOne.contenu.main")}{" "}
        <code className="font-mono font-medium text-sky-500 dark:text-sky-400">
          {t("sectionOne.contenu.highlight")}
        </code>
        ,{t("sectionOne.contenu.mainBis")}
      </p>
      <div className="flex justify-center gap-6 mt-10 text-sm">
        <a
          className="flex items-center h-12 px-6 text-sm font-semibold text-white rounded-lg pointer-events-auto bg-slate-900 hover:bg-slate-700 focus:outline-none dark:bg-slate-700 dark:hover:bg-slate-600"
          href=""
        >
          {t("sectionOne.search.searchBtn")}
        </a>
      </div>
    </div>
  );
};

export default  HeroSection ;
