import { useTranslation } from "react-i18next";
import YBUserIcon from "../../../../components/YBUserIcon/YBUserIcon";
import { useStore } from "../../../../stores/store";

const YBProfil = () => {
  const { userStore } = useStore();
  const { t } = useTranslation();
  return (
    <div className="max-w-3xl mx-auto py-8 md:py-16 lg:py-40">
      <div className="p-8 bg-white dark:bg-gray-800 shadow md:mb-0 rounded-2xl">
        <div className="grid grid-cols-1 md:grid-cols-3">
          <div className="grid grid-cols-3 text-center order-last md:order-first mt-20 md:mt-0">
            <div>
              <p className="font-bold text-gray-700 dark:text-gray-400 text-xl">22</p>
              <p className="text-gray-400 dark:text-gray-100">Friends</p>
            </div>
            <div>
              <p className="font-bold text-gray-700 dark:text-gray-400 text-xl">10</p>
              <p className="text-gray-400 dark:text-gray-100">Photos</p>
            </div>
            <div>
              <p className="font-bold text-gray-700 dark:text-gray-400 text-xl">89</p>
              <p className="text-gray-400 dark:text-gray-100">Comments</p>
            </div>
          </div>
          <div className="relative">
            <div className="mx-auto absolute inset-x-0 top-0 -mt-24 flex items-center justify-center">
              <YBUserIcon className="bg-white dark:bg-gray-800" status={false} size="Xlarge"/>
            </div>
          </div>

          <div className="gap-4 flex justify-between mt-32 md:mt-0 md:justify-center">
          <button
                  onClick={userStore.logout}
                  className="flex items-center h-12 px-6 text-sm font-semibold text-white rounded-lg pointer-events-auto bg-slate-900 hover:bg-slate-700 focus:outline-none dark:bg-slate-700 dark:hover:bg-slate-600"
                >
                  {t("login.Logout")}
                </button>
          </div>
        </div>

        <div className="mt-20 text-center border-b  pb-12">
          <h1 className="text-4xl font-medium text-gray-700 dark:text-gray-100">
            Yahya BENABDALLAH
          </h1>
          <p className="font-light text-gray-600 mt-3  dark:text-gray-200">Oujda, Maroc</p>

          <p className="mt-2 text-gray-500 dark:text-gray-300">Dev - Front-End</p>
          <p className="mt-1 text-gray-500 dark:text-gray-400">Remote</p>
        </div>

        <div className="mt-12 flex flex-col justify-center">
          <p className="text-gray-600 dark:text-gray-400 text-center font-light lg:px-16">
            An artist of considerable range, Ryan — the name taken by Melbourne-raised,
            Brooklyn-based Nick Murphy — writes, performs and records all of his own music, giving
            it a warm, intimate feel with a solid groove structure. An artist of considerable range.
          </p>
        </div>
      </div>
    </div>
  );
};

export default YBProfil;
