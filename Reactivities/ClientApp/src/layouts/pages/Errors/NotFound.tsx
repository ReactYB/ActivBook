import { useTranslation } from "react-i18next";
import { NavLink } from "react-router-dom";

const NotFound = () => {
  const {t} = useTranslation();
  return (
    <div className="max-w-7xl mx-auto lg:px-24 md:px-44 h-screen px-4 pt-24 flex justify-center flex-col-reverse lg:flex-row md:gap-28 gap-16">
      <div className="relative w-full pb-12 xl:pt-24 xl:w-1/2 lg:pb-0">
        <div className="">
          <div className="">
            <h1 className="my-2 text-2xl font-bold text-gray-800 dark:text-gray-300">
              {t("404.title")}
            </h1>
            <p className="my-2 text-gray-800 dark:text-gray-300">{t("404.contenu")}</p>
            <NavLink              
              to={"/activities"}
              className="inline-block my-2 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-3 md:mr-0 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              {t("404.goBack")}
            </NavLink>
          </div>
          <img className="mt-4" src="https://i.ibb.co/G9DC8S0/404-2.png" alt="" />
        </div>
      </div>
      <div>
        <img src="https://i.ibb.co/ck1SGFJ/Group.png" alt="" />
      </div>
    </div>
  );
};

export default NotFound;
