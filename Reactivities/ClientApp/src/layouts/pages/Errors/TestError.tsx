import axios from "axios";
import { useState } from "react";
import { toast } from "react-toastify";
import ValidationErrors from "./ValidationErrors";

export default function TestError() {
  const baseUrl = "http://localhost:5000/api/";
  const [errors, SetErrors] = useState(null);
  function handleNotFound() {
    axios.get(baseUrl + "buggy/not-found").catch((err) => console.log(err.response));
  }

  function handleBadRequest() {
    axios.get(baseUrl + "buggy/bad-request").catch((err) => console.log(err.response));
  }

  function handleServerError() {
    axios.get(baseUrl + "buggy/server-error").catch((err) => console.log(err.response));
  }

  function handleUnauthorised() {
    axios.get(baseUrl + "buggy/unauthorised").catch((err) => console.log(err.response));
  }

  function handleBadGuid() {
    axios.get(baseUrl + "activities/notaguid").catch((err) => console.log(err));
  }

  function handleValidationError() {
    axios.post(baseUrl + "activities", {}).catch((err) => SetErrors(err));
  }

  return (
    <div className="h-screen mx-auto mt-5 max-w-7xl">
      <h1 className="text-xl text-center">Test Error component</h1>
      <div className="flex justify-between mx-2 mt-5 justify-items-center">
        <button
          className="px-4 py-2 font-bold text-white bg-orange-500 rounded hover:bg-orange-700"
          onClick={handleNotFound}
        >
          Not Found
        </button>
        <button
          className="px-4 py-2 font-bold text-white rounded bg-slate-500 hover:bg-slate-700"
          onClick={handleBadRequest}
        >
          Bad Request
        </button>
        <button
          className="px-4 py-2 font-bold text-white bg-purple-500 rounded hover:bg-purple-700"
          onClick={handleValidationError}
        >
          Validation Error
        </button>
        <button
          className="px-4 py-2 font-bold text-white bg-red-500 rounded hover:bg-red-700"
          onClick={handleServerError}
        >
          Server Error
        </button>
        <button
          className="px-4 py-2 font-bold text-white rounded bg-amber-600 hover:bg-amber-700"
          onClick={handleUnauthorised}
        >
          Unauthorised
        </button>
        <button
          className="px-4 py-2 font-bold text-white bg-blue-500 rounded hover:bg-blue-700"
          onClick={handleBadGuid}
        >
          Bad Guid
        </button>
      </div>
      {errors && (
        <div className="container">
          <ValidationErrors errors={errors} />
        </div>
      )}
    </div>
  );
}
