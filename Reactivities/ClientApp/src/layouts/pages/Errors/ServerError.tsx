import { observer } from "mobx-react-lite";
import { useStore } from "../../../stores/store";

const ServerError = () => {
  const { commonStore } = useStore();
  return (
    <>
      <div className="mx-auto mt-5 max-w-7xl">
        <h2 className="text-xl text-violet-600">Server Error</h2>
        <h5 className="text-red-600">{commonStore.error?.message}</h5>
        <div className="text-red-400">
          {commonStore.error?.details && (
            <>
              <h6 className="">Stack Trace</h6>
              <code className="">{commonStore.error.details}</code>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default observer(ServerError);
