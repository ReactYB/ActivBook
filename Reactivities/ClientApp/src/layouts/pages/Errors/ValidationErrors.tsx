interface Props {
  errors: string[] | null;
}
const ValidationErrors = ({ errors }: Props) => {
  return (
    <>
      <div>ValidationErrors</div>
      <ul>
        {errors?.map((error: any, index) => (
          <li key={index}>{error}</li>
        ))}
      </ul>
    </>
  );
};

export default ValidationErrors;
