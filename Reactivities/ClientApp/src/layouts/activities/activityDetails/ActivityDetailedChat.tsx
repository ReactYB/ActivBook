import { observer } from "mobx-react-lite";

const ActivityDetailedChat = () => {
  return (
    <>
      <div className="px-4 mb-5">
        <div className="relative grid grid-cols-1 bg-white border rounded-lg ">
          <div className="relative flex gap-2">
            <img
              src="/assets/laptop.svg"
              className="relative object-contain w-16 h-16 p-3 bg-white border rounded-full -top-4 -left-2"
              alt=""
              loading="lazy"
            />
            <div className="flex flex-col w-full">
              <div className="flex flex-row justify-between">
                <p className="relative text-lg truncate">Nom Prenom</p>
              </div>
              <p className="text-sm text-gray-400">
                20 April 2022, at 14:88 PM
              </p>
            </div>
          </div>
          <p className="px-4 mb-5 text-justify text-gray-500 ">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime
            quisquam vero adipisci beatae voluptas dolor ame. Lorem ipsum dolor
            sit amet consectetur adipisicing elit. Maxime quisquam vero adipisci
            beatae voluptas dolor ame. Lorem ipsum dolor sit amet consectetur
            adipisicing elit. Maxime quisquam vero adipisci beatae voluptas
            dolor ame.Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Maxime quisquam vero adipisci beatae voluptas dolor ame. Lorem ipsum
            dolor sit amet consectetur adipisicing elit. Maxime quisquam vero
            adipisci beatae voluptas dolor ame.Lorem ipsum dolor sit amet
            consectetur adipisicing elit. Maxime quisquam vero adipisci beatae
            voluptas dolor ame.
          </p>
        </div>
      </div>
      <div className="flex items-center justify-center mx-auto mt-5 mb-10">
        <form className="w-full px-4 pt-2 bg-white rounded-lg">
          <div className="flex flex-wrap mb-6 -mx-3">
            <h2 className="px-4 pt-3 pb-2 text-lg text-gray-800">
              Add a new comment
            </h2>
            <div className="w-full px-4 mt-2 mb-2 md:w-full">
              <textarea
                className="w-full h-20 px-2 py-2 font-medium leading-normal placeholder-gray-700 bg-gray-100 border border-gray-400 rounded resize-none focus:outline-none focus:bg-white"
                name="body"
                placeholder="Type Your Comment"
                required
              ></textarea>
            </div>
            <div className="flex items-start w-full px-4 md:w-full">
              <div className="">
                <input
                  type="submit"
                  className="px-4 py-2 font-bold text-center text-white bg-gray-400 border border-gray-400 rounded-lg cursor-pointer hover:bg-gray-500"
                  value="Post Comment"
                />
              </div>
            </div>
          </div>
        </form>
      </div>
    </>
  );
};

export default observer(ActivityDetailedChat);
