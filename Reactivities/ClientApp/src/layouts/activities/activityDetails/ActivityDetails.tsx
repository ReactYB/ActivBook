import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import ActivityDetailedChat from "./ActivityDetailedChat";
import ActivityDetailedHeader from "./ActivityDetailedHeader";
import ActivityDetailedInfo from "./ActivityDetailedInfo";
import ActivityDetailedSidebar from "../components/ActivitySidebar";
import { useStore } from "../../../stores/store";
import YBLoadingMain from "../../../components/YBLoading/YBLoadingMain";

const ActivityDetails = () => {
  const { activityStore } = useStore();

  const {
    selectedActivity: activity,
    loadActivity,
    loadingInitial,
  } = activityStore;
  const { id } = useParams<{ id: string }>();
  useEffect(() => {
    if (id) {
      loadActivity(id)
    }
  }, [id, loadActivity]);
  if (loadingInitial || !activity)
    return (
      <>
        <YBLoadingMain />
      </>
    );
  return (
    <>
      <div className="mx-auto mt-5 max-w-7xl">
        <div className="grid grid-cols-12 gap-4">
          <div className="col-span-8 shadow-2xl rounded-t-2xl">
            <ActivityDetailedHeader activity={activity} />
            <ActivityDetailedInfo activity={activity} />
            <ActivityDetailedChat />
          </div>
          <div className="col-span-4">
            <ActivityDetailedSidebar />
          </div>
        </div>
      </div>
    </>
  );
};

export default observer(ActivityDetails);
