import { format } from "date-fns";
import { observer } from "mobx-react-lite";
import { Activity } from "../../../models/Activity";

interface Props {
  activity: Activity ;
}

const ActivityDetailedInfo = ({ activity }: Props) => {
  return (
    <>
      <div className="p-4 mb-3 ">
        <ul className="list-inside">
          <li className="font-bold">{activity.description}</li>
          <li className="text-slate-400">
            {format(activity.date!, "dd MMM yyyy  h:mm aa")}
          </li>
          <li className="text-slate-400">
            {activity.venue}, {activity.city}
          </li>
        </ul>
      </div>
    </>
  );
};

export default observer(ActivityDetailedInfo);
