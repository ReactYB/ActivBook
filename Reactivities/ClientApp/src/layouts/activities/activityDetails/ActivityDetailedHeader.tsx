import { format } from "date-fns";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";
import { Activity } from "../../../models/Activity";


interface Props {
  activity: Activity ;
}

const ActivityDetailedHeader = ({ activity }: Props) => { 
  return (
    <>
      <div className="relative">
        <img
          className="object-cover rounded-t-2xl"
          src={`/assets/categoryImages/${activity.category}.jpg`}
          alt="..."
        />
        <div className="absolute bottom-0 left-0 right-0 p-4 text-white bg-gradient-to-t from-slate-900">
          <h2>{activity.title}</h2>
          <p>{format(activity.date!, "dd MMM yyyy  h:mm aa")}</p>
          <p>
            Hosted by <strong>Bob</strong>
          </p>
        </div>
      </div>
      <div className="grid grid-cols-3 gap-3 px-4 mt-3">
        <button className="px-4 py-2 font-bold text-white bg-blue-500 rounded hover:bg-blue-700">
          Join Activity
        </button>
        <button className="px-4 py-2 font-bold text-white rounded bg-amber-600 hover:bg-amber-700">
          Cancel attendance
        </button>
        <Link
          type="button"
          className="px-4 py-2 font-bold text-center text-white bg-purple-500 rounded hover:bg-purple-700"
          to={`/manage/${activity.id}`}
        >
          Manage Event
        </Link>
      </div>
    </>
  );
};

export default observer(ActivityDetailedHeader);
