import { useEffect } from "react";
import YBAccordion from "../../../components/YBAccordion/YBAccordion";
import YBLoading from "../../../components/YBLoading/YBLoadingMain";
import TestimonialSlider from "../../pages/Home/Components/TestimonialSlider";
import ActivityList from "../activityList/ActivityList";
import HeroSection from "../../pages/Home/Components/HeroSection";
import {
  useTypeDispatch,
  useTypeSelector,
} from "../../../Redux/StoreReduxConfig";
import { fetchAllProductsAsync } from "../../../Redux/Slices/activitySlice";

const ActivityDashboard = () => {
  const { activityLoaded } = useTypeSelector(
    (state) => state.activity
  );
  const dispatch = useTypeDispatch();

  useEffect(() => {
    if (!activityLoaded) {
      dispatch(fetchAllProductsAsync());
    }
  }, [activityLoaded, dispatch]);

  if (!activityLoaded) return <YBLoading />;

  return (
    <>
      <HeroSection />
      <ActivityList />
      <TestimonialSlider />
      <YBAccordion />
    </>
  );
};

export default ActivityDashboard;