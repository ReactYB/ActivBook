import { useTranslation } from "react-i18next";
import { Activity } from "../../../models/Activity";
import ActivityListItem from "../components/ActivityListItem";
import { useTypeSelector } from "../../../Redux/StoreReduxConfig";

const ActivityList = () => {
 const { activities } = useTypeSelector(
   (state) => state.activity
 );
  const { t } = useTranslation();

  return (
    <>
      <div className="relative max-h-[42rem] overflow-hidden pt-20 mx-auto sm:pt-24 lg:pt-32">
        <div className="max-w-7xl mx-auto grid grid-cols-1 sm:grid-cols-2 gap-4 md:grid-cols-3">
          {activities &&
            activities
              .slice(0, 5)
              .map((activity: Activity) => (
                <ActivityListItem key={activity.id} activity={activity} />
              ))}
        </div>
        <div className="absolute inset-0 h-full flex justify-center pt-64 pb-16 bg-gradient-to-t from-white/60 dark:from-slate-900">
          <button
            type="button"
            className="mt-auto flex items-center h-12 px-6 text-sm font-semibold text-white rounded-lg pointer-events-auto bg-slate-900 hover:bg-slate-700 focus:outline-none dark:bg-slate-700 dark:hover:bg-slate-600"
          >
            {t("sectionTwo.showMore")}
          </button>
        </div>
      </div>
    </>
  );
};
export default ActivityList;
