import { Form, Formik } from "formik";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import * as Yup from "yup";
import { v4 } from "uuid";
import { useStore } from "../../../stores/store";
import { Activity } from "../../../models/Activity";
import YBLoadingMain from "../../../components/YBLoading/YBLoadingMain";
import MyTextInput from "../../../components/YBInputs/form/MyTextInput";
import MyTextArea from "../../../components/YBInputs/form/MyTextArea";
import MySelectInput from "../../../components/YBInputs/form/MySelectInput";
import MyDateInput from "../../../components/YBInputs/form/MyDateInput";
import { useTranslation } from "react-i18next";

 const categoryOptions = [
  { text: "Drinks", value: "drinks" },
  { text: "Culture", value: "culture" },
  { text: "Film", value: "film" },
  { text: "Food", value: "food" },
  { text: "Music", value: "music" },
  { text: "Travel", value: "travel" },
];

const ActivityForm = () => {
  const History = useHistory();
  const {t} = useTranslation();
  const { activityStore } = useStore();
  const { createActivity, updateActivity, loadingInitial, loadActivity, setLoadingInitial } =
    activityStore;
  const validationSchema = Yup.object({
    title: Yup.string()
      .required(t("validation.generique").toString())
      .matches(/^[aA-zZ\s]+$/, t("validation.text").toString()),
    description: Yup.string()
      .required(t("validation.generique").toString())
      .matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this MyTextInput "),
    category: Yup.string().required(t("validation.generique").toString()),
    date: Yup.string().required(t(t("validation.generique").toString()).toString()),
    city: Yup.string().required(t("validation.generique").toString()),
    venue: Yup.string().required(t("validation.generique").toString()),
  });
  const [activity, setActivity] = useState<Activity>({
    id: "",
    title: "",
    category: "",
    description: "",
    date: new Date(),
    city: "",
    venue: "",
  });

  const { id } = useParams<{ id: string }>();
  useEffect(() => {
    if (id) {
      loadActivity(id).then((activityResp) => {
        setActivity(activityResp!);
      });
    } else {
      setLoadingInitial(false);
    }
  }, [id, loadActivity, setLoadingInitial]);
  const handleFormSubmit = (activityForm: Activity) => {
    if (activity.id.length === 0) {
      let newActivity = {
        ...activityForm,
        id: v4(),
      };
      createActivity(newActivity).then(() => {
        History.push(`/activities/${newActivity.id}`);
      });
    } else {
      updateActivity(activityForm).then(() => {
        History.push(`/activities/${activity.id}`);
      });
    }
  };
  if (loadingInitial)
    return (
      <>
        <YBLoadingMain/>
      </>
    );

  return (
    <>
      {loadingInitial ? (
        <YBLoadingMain />
      ) : (
        <div className="my-8 lg:my-12 xl:my-24">
          <Formik
            validationSchema={validationSchema}
            enableReinitialize
            initialValues={activity}
            onSubmit={(values) => handleFormSubmit(values)}
          >
            {({ handleSubmit, isValid, isSubmitting, dirty }) => (
              <Form
                className="max-w-2xl mt-16 p-8 mx-auto bg-white shadow-2xl dark:bg-gray-800 rounded-2xl"
                onSubmit={handleSubmit}
                autoComplete="off"
              >
                <div className="py-5 mb-10 -mt-16 text-white bg-primary dark:bg-primaryDark rounded-lg">
                  <h1 className="text-xl font-bold leading-tight tracking-tight text-center text-white md:text-2xl ">
                    {t("login.title")}

                  </h1>
                </div>
                <MyTextInput placeholder={t("inputs.title")} name="title" />
                <MyTextArea row={4} placeholder={t("inputs.description")}  name="description" />
                <MySelectInput option={categoryOptions} placeholder={t("inputs.category")}  name="category" />
                <MyDateInput
                  placeholderText="Date"
                  name="date"
                  showTimeSelect
                  timeCaption="time"
                  dateFormat="MMM d, yyyy h:mm aa"
                />

                <MyTextInput placeholder={t("inputs.city")}  name="city" />

                <MyTextInput placeholder={t("inputs.venue")}  name="venue" />

                <div className="clearfix">
                  <div className="grid grid-cols-2 gap-3 mt-10">
                    <button
                      className={
                        "bg-blue-500 disabled:opacity-40 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                      }
                      type="submit"
                      disabled={!dirty || !isValid || isSubmitting}
                    >
                      {!dirty || !isValid || isSubmitting ? t("login.submit") : t("login.submit")+" ..."}
                    </button>
                    <button
                      onClick={() => History.goBack()}
                      className="px-4 py-2 font-bold text-white bg-orange-500 rounded hover:bg-orange-600"
                    >
                      {t("login.cancel")}
                    </button>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      )}
    </>
  );
};

export default observer(ActivityForm);
