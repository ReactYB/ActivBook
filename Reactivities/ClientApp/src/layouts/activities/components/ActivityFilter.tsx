import Calendar from "react-calendar";

const ActivityFilters = () => {
  return (
    <div className="">
      {/* <div className="mb-2 card" style={{ marginTop: "36px" }}>
        <div className="card-header">
          <div>Tom</div>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            <strong>Hostiong</strong>
          </li>
          <li className="list-group-item text-muted">01/10/2022</li>
          <li className="list-group-item">A third item</li>
        </ul>
      </div> */}
      <div className="w-full mt-5">
        <span className="block w-full px-2 py-5 font-semibold text-center capitalize border rounded-2xl bg-neutral-100">
          Filter By date
        </span>
      </div>
      <Calendar className="mt-5 text-sm border shadow-2xl rounded-2xl bg-neutral-100 rounded-t-2xl lg:w-full 2xl:w-full " />
    </div>
  );
};

export default ActivityFilters;
