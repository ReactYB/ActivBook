import { observer } from "mobx-react-lite";

const ActivitySidebar = () => {
  const words = [
    { name: "word1" },
    { name: "word1" },
    { name: "word1" },
    { name: "word1" },
  ]; //max 4
  const pdfs = [
    { name: "pdf1" },
    { name: "pdf1" },
    { name: "pdf1" },
    { name: "pdf1" },
  ]; //max 4
  const excels = [
    { name: "excel1" },
    { name: "excel1" },
    { name: "excel1" },
    { name: "excel1" },
  ]; //max 4
  return (
    <>
      <div className="grid gap-2 ">
        <div className="grid grid-flow-col grid-cols-12 grid-rows-4 gap-4 py-3 bg-white rounded-lg shadow-lg">
          <div className="col-span-2 row-span-4 my-auto bg">
            <img
              className="object-contain p-2"
              src="/assets/pdf.svg"
              alt="..."
            />
          </div>
          {pdfs.map((pdf) => (
            <div className="row-span-1 col-span-10 ...">{pdf.name}</div>
          ))}
        </div>
        <div className="grid grid-flow-col grid-cols-12 grid-rows-4 gap-4 py-3 bg-white rounded-lg shadow-lg">
          <div className="col-span-2 row-span-4 my-auto">
            <img
              className="object-contain p-2"
              src="/assets/excel.svg"
              alt="..."
            />
          </div>
          {excels.map((excel) => (
            <div className="row-span-1 col-span-10 ...">{excel.name}</div>
          ))}
        </div>
        <div className="grid grid-flow-col grid-cols-12 grid-rows-4 gap-4 py-3 bg-white rounded-lg shadow-lg">
          <div className="col-span-2 row-span-4 my-auto">
            <img
              className="object-contain p-2"
              src="/assets/word.svg"
              alt="..."
            />
          </div>
          {words.map((word) => (
            <div className="row-span-1 col-span-10 ...">{word.name}</div>
          ))}
        </div>
      </div>
    </>
  );
};

export default observer(ActivitySidebar);
