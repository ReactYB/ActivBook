import { format } from "date-fns/esm";
import Excel from "../../../components/YBIcons/Excel";
import { Activity } from "../../../models/Activity";
import parseISO from "date-fns/parseISO";

interface Props {
  activity: Activity;
}

const ActivityListItem = ({ activity }: Props) => {
  return (
    <>
      <div className="mx-1 text-gray-900 bg-transparent rounded-xl lg:mx-0">
        <div className="flex flex-row-reverse group hover:translate-y-3 translate-y-6 duration-300 ease-[cubic-bezier(0.25, 0.46, 0.45, 0.94)] bg-gray dark:bg-sandy bg-primary rounded-t-xl cursor-pointer">
          <div className="relative w-16 h-16 group-hover:-translate-y-3 delay-300 duration-300 ease-[cubic-bezier(0.25, 0.46, 0.45, 0.94)]">
            <div className="absolute inset-0 mt-3 rtl:ml-3 ltr:mr-1 rtl:rotate-[35deg] ltr:rotate-[35deg]">
              <Excel />
            </div>
          </div>
        </div>
        <div className="relative p-4 overflow-hidden bg-gray-300 shadow-lg card-desc rounded-xl dark:bg-gray-700 dark:text-gray-400 ">
          <div className="flex">
            <div className="w-11/12">
              <div className="overflow-hidden text-xl font-semibold dark:text-gray-200 text-ellipsis whitespace-nowrap">
                {activity.venue}
              </div>
              <div className="overflow-hidden text-ellipsis whitespace-nowrap">
                {format(parseISO(activity.date!), "dd MMM yyyy  h:mm aa")}
              </div>
              <div className="overflow-hidden text-ellipsis whitespace-nowrap">
                {activity.venue}
              </div>
              <div className="overflow-hidden text-ellipsis whitespace-nowrap">{activity.city}</div>
            </div>
            <div className="flex flex-col w-1/12">
              <button type="button" onClick={() => {}} className="flex-1">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="dark:text-primaryDark text-primary w-5 h-5 mx-auto my-1 rounded-full hover:opacity-75 "
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M13.19 8.688a4.5 4.5 0 011.242 7.244l-4.5 4.5a4.5 4.5 0 01-6.364-6.364l1.757-1.757m13.35-.622l1.757-1.757a4.5 4.5 0 00-6.364-6.364l-4.5 4.5a4.5 4.5 0 001.242 7.244"
                  />
                </svg>
              </button>
              <button type="button" onClick={() => {}} className="flex-1">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="dark:text-primaryDark text-primary w-5 h-5 mx-auto my-1 rounded-full hover:opacity-75 "
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M17.593 3.322c1.1.128 1.907 1.077 1.907 2.185V21L12 17.25 4.5 21V5.507c0-1.108.806-2.057 1.907-2.185a48.507 48.507 0 0111.186 0z"
                  />
                </svg>
              </button>
              <button type="button" onClick={() => alert("hi") } className="flex-1">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="dark:text-primaryDark text-primary  w-5 h-5 mx-auto my-1 rounded-full hover:opacity-75 "
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M9.594 3.94c.09-.542.56-.94 1.11-.94h2.593c.55 0 1.02.398 1.11.94l.213 1.281c.063.374.313.686.645.87.074.04.147.083.22.127.324.196.72.257 1.075.124l1.217-.456a1.125 1.125 0 011.37.49l1.296 2.247a1.125 1.125 0 01-.26 1.431l-1.003.827c-.293.24-.438.613-.431.992a6.759 6.759 0 010 .255c-.007.378.138.75.43.99l1.005.828c.424.35.534.954.26 1.43l-1.298 2.247a1.125 1.125 0 01-1.369.491l-1.217-.456c-.355-.133-.75-.072-1.076.124a6.57 6.57 0 01-.22.128c-.331.183-.581.495-.644.869l-.213 1.28c-.09.543-.56.941-1.11.941h-2.594c-.55 0-1.02-.398-1.11-.94l-.213-1.281c-.062-.374-.312-.686-.644-.87a6.52 6.52 0 01-.22-.127c-.325-.196-.72-.257-1.076-.124l-1.217.456a1.125 1.125 0 01-1.369-.49l-1.297-2.247a1.125 1.125 0 01.26-1.431l1.004-.827c.292-.24.437-.613.43-.992a6.932 6.932 0 010-.255c.007-.378-.138-.75-.43-.99l-1.004-.828a1.125 1.125 0 01-.26-1.43l1.297-2.247a1.125 1.125 0 011.37-.491l1.216.456c.356.133.751.072 1.076-.124.072-.044.146-.087.22-.128.332-.183.582-.495.644-.869l.214-1.281z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ActivityListItem;
