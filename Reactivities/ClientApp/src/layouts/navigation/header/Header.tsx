/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import {
  AtSymbolIcon,
  Bars3BottomRightIcon,
  ChevronDownIcon,
  PlayIcon,
  ShieldCheckIcon,
  XMarkIcon,
} from "@heroicons/react/20/solid";
import { NavLink } from "react-router-dom";
import { useStore } from "../../../stores/store";
import { observer } from "mobx-react-lite";
import YBToggleDark from "../../../components/YBToggleDark/YBToggleDark";
import YBToggleLang from "../../../components/YBToggleLang/YBToggleLang";
import { useTranslation } from "react-i18next";
import YBLogo from "../../../components/YBLogo/YBLogo";

const solutions = [
  {
    name: "Soka Collection 2015",
    description: "Speak directly to your customers in a more meaningful way.",
    href: "#",
    icon: AtSymbolIcon,
  },
  {
    name: "Soka Collection 2016",
    description: "Your customers' data will be safe and secure.",
    href: "#",
    icon: ShieldCheckIcon,
  },
  {
    name: "Resort 2016 Collection",
    description: "Connect with third-party tools that you're already using.",
    href: "#",
    icon: AtSymbolIcon,
  },
];

const callsToAction = [{ name: "SEE ALL COLLECTIONS", href: "#", icon: PlayIcon }];

// Safer Mapping null undefined removed
function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

const Header = () => {
  const { modalStore, userStore } = useStore();
  const { t } = useTranslation();

  return (
    <Popover className="h-[70px] sticky top-0 z-40 w-full px-2 py-1 transition-colors duration-200 ease-in-out rounded-b-lg bg-gradient-to-b from-gray-200/80 dark:from-gray-900/60 backdrop-blur sm:px-4">
      {({ open }) => (
        <>
          {/* top layer for mobile */}
          <div className="absolute inset-0 z-30 pointer-events-none" aria-hidden="true" />
          {/* top layer for desoktop */}
          <div className="relative z-20">
            <div className="flex items-center justify-between px-1 py-3 mx-auto max-w-7xl">
              {/* ICON container DESOKTOP */}
              <NavLink to={"/"} className="flex gap-2">
                <span className="sr-only">ChamssDoha</span>
                <div className="w-10">
                  <YBLogo />
                </div>
                <span className="self-center block text-xl font-semibold md:hidden lg:block whitespace-nowrap dark:text-white">
                  {t("logo")}
                </span>
              </NavLink>

              {/* OPEN MENU container MOBILE*/}
              <div className="-my-2 -mr-2 md:hidden">
                <Popover.Button className="inline-flex items-center justify-center p-2 text-gray-400 rounded-md hover:text-gray-500 hover:bg-gray-100 dark:hover:text-gray-400 dark:hover:bg-gray-700/80 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-slate-400">
                  <span className="sr-only">Open menu</span>
                  <Bars3BottomRightIcon className="w-6 h-6" aria-hidden="true" />
                </Popover.Button>
              </div>

              {/*  MENU ITEMS DESKTOP */}
              <div className="relative hidden md:flex-1 md:flex md:items-center md:justify-between ">
                <Popover.Group
                  as="nav"
                  className="flex gap-12 rtl:sm:mr-16 ltr:sm:ml-16 rtl:lg:mr-32 ltr:lg:ml-32"
                >
                  {/*  COLLECTION PopOver ITEMS DESKTOP */}
                  <Popover>
                    {({ open }) => (
                      <>
                        <Popover.Button className="relative inline-flex items-center font-semibold text-gray-900 group hover:bg-gray-100 md:hover:bg-transparent md:hover:text-black md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                          {/*  COLLECTION BUTTON*/}
                          <span className="self-center">{t("header.linkOne")}</span>
                          <ChevronDownIcon
                            className={classNames(
                              open ? "text-gray-900 rotate-180" : "text-gray-400",
                              "rtl:mr-2 ltr:ml-2 mt-1 h-5 w-5 group-hover:text-gray-500 transition-transform delay-150 duration-150"
                            )}
                            aria-hidden="true"
                          />
                        </Popover.Button>

                        <Transition
                          show={open}
                          as={Fragment}
                          enter="transition ease-out duration-200"
                          enterFrom="opacity-0 -translate-y-1"
                          enterTo="opacity-100 translate-y-0"
                          leave="transition ease-in duration-150"
                          leaveFrom="opacity-100 translate-y-0"
                          leaveTo="opacity-0 -translate-y-1"
                        >
                          {/*  COLLECTION ITEMS*/}
                          <Popover.Panel
                            static
                            className="absolute inset-x-0 z-10 hidden max-w-sm mt-4 transform shadow-lg rounded-xl md:block bg-slate-800"
                          >
                            {/*  COLLECTION GRID */}
                            <div className="mx-auto">
                              {solutions.map((item) => (
                                /*  COLLECTION LINK */
                                <a
                                  key={item.name}
                                  href={item.href}
                                  className="flex flex-col p-3 m-3 rounded-lg lg:justify-between"
                                >
                                  <div className="flex">
                                    {/*  COLLECTION ITEM IMAGE */}

                                    <span className="flex items-center justify-center p-2 text-white bg-indigo-500 rounded-md">
                                      <item.icon className="w-6 h-6" aria-hidden="true" />
                                    </span>

                                    {/*  COLLECTION ITEM DESCRIPTION */}
                                    <div className="flex items-center ml-3">
                                      <div>
                                        <p className="text-base font-medium text-gray-900 uppercase md:text-xl dark:text-white">
                                          {item.name}
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                </a>
                              ))}
                            </div>
                            {/*  COLLECTION SEE MORE */}
                            <div className="bg-gray-50">
                              <div className="flex px-4 py-5 mx-auto space-y-6 text-center max-w-7xl sm:flex sm:space-y-0 sm:space-x-10 sm:px-6 lg:px-8 md:items-center md:justify-center">
                                {callsToAction.map((item) => (
                                  <div key={item.name} className="flow-root ">
                                    <a
                                      href={item.href}
                                      className="flex items-center p-3 -m-3 text-base font-medium text-gray-900 rounded-md hover:bg-gray-100"
                                    >
                                      <item.icon
                                        className="flex-shrink-0 w-6 h-6 text-gray-400"
                                        aria-hidden="true"
                                      />
                                      <span className="ml-3">{item.name}</span>
                                    </a>
                                  </div>
                                ))}
                              </div>
                            </div>
                          </Popover.Panel>
                        </Transition>
                      </>
                    )}
                  </Popover>
                  <NavLink
                    to={"/activities"}
                    type="button"
                    className={(isActive) =>
                      "block text-gray-700 font-semibold hover:bg-gray-100 md:hover:bg-transparent md:hover:text-black md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700" +
                      (isActive ? " text-primary/95 dark:text-primaryDark" : "")
                    }
                    aria-expanded="false"
                  >
                    <span className="self-center truncate">{t("header.linkTwo")}</span>
                  </NavLink>
                  <NavLink
                    to={"/createActivity"}
                    type="button"
                    className={(isActive) =>
                      "block py-2 pr-4 pl-3 font-semibold text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-black md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700" +
                      (isActive ? " text-primary/95 dark:text-primaryDark" : "")
                    }
                    aria-expanded="false"
                  >
                    <span className="self-center truncate">{t("header.linkThree")}</span>
                  </NavLink>
                </Popover.Group>

                <div className="flex items-center rtl:md:mr-12 ltr:md:ml-12">
                  {!userStore.isLoggedIn ? (
                    <>
                      {" "}
                      <NavLink
                        to={"/register"}
                        className={(isActive) =>
                          "block truncate text-gray-700 font-semibold hover:bg-gray-100 md:hover:bg-transparent md:hover:text-black md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700" +
                          (isActive ? " text-primary/95 dark:text-primaryDark" : "")
                        }
                      >
                        {t("login.signUp")}
                      </NavLink>
                      <button
                        onClick={modalStore.openModal}
                        className="inline-flex items-center self-center justify-center px-4 py-2 text-base font-medium text-white truncate border border-transparent rounded-md shadow-sm  rtl:mr-8 ltr:ml-8 bg-primary hover:bg-primary/75 dark:bg-primaryDark dark:hover:bg-primaryDark/75"
                      >
                        {t("login.signIn")}
                      </button>
                    </>
                  ):<NavLink
                  to={"/Profil"}
                  className=
                    "inline-flex items-center self-center justify-center px-4 py-2 text-base font-medium text-white truncate border border-transparent rounded-md shadow-sm rtl:mr-8 ltr:ml-8 bg-primary hover:bg-primary/75 dark:bg-primaryDark dark:hover:bg-primaryDark/75"
                >
                  {t("login.Profile")}
                </NavLink>}
                  <YBToggleDark className="rtl:mr-3 ltr:ml-3" />
                  <YBToggleLang className="rtl:mr-3 ltr:ml-3" />
                </div>
              </div>
            </div>
          </div>
          {/* Pop over Mobile */}
          <Transition
            show={open}
            as={Fragment}
            enter="duration-200 ease-out"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="duration-100 ease-in"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <Popover.Panel
              focus
              static
              className="absolute inset-x-0 top-0 z-30 transition origin-top-right transform md:hidden"
            >
              <div className="px-2 py-1 border-t border-gray-200 rounded-b-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-gray-50 border-x dark:border-gray-700 dark:bg-gray-800 ">
                <div className="px-1 py-3">
                  <div className="flex items-center justify-between">
                    <div className="w-10">
                      <YBLogo />
                    </div>
                    <div className="-mr-2">
                      <Popover.Button className="inline-flex items-center justify-center p-2 text-gray-400 rounded-md hover:text-gray-500 hover:bg-gray-100 dark:hover:text-gray-400 dark:hover:bg-gray-700/80 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-slate-400">
                        <span className="sr-only">Close menu</span>
                        <XMarkIcon className="w-6 h-6" aria-hidden="true" />
                      </Popover.Button>
                    </div>
                  </div>
                  <div className="flex items-center justify-center">
                    <YBToggleDark className="rtl:mr-3 ltr:ml-3" />
                    <YBToggleLang className="rtl:mr-3 ltr:ml-3" />
                  </div>
                </div>
              </div>
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  );
};
export default Header;
