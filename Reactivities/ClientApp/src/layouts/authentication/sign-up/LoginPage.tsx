import { Form, Formik } from "formik";
import { observer } from "mobx-react-lite";
import { useTranslation } from "react-i18next";
import MyTextInput from "../../../components/YBInputs/form/MyTextInput";
import { useStore } from "../../../stores/store";
import * as Yup from "yup";
import { NavLink } from "react-router-dom";

const SignIn = () => {
  const { t } = useTranslation();
  const { userStore } = useStore();
  const validationSchema = Yup.object({
    email: Yup.string()
      .required(t("validation.email").toString())
      .email(),
    password: Yup.string()
      .required( t("validation.password").toString())
  });

  return (
    <section>
      <div className="px-4 py-12 mx-auto max-w-7xl sm:px-6 md:px-12 lg:px-16 lg:py-24">
        <div className="justify-center mx-auto align-bottom transition-all transform bg-white dark:bg-gray-900/40 rounded-lg sm:align-middle sm:max-w-4xl sm:w-full">
          <div className="grid flex-wrap items-center justify-center grid-cols-1 mx-auto shadow-xl lg:grid-cols-2 rounded-xl">
            <div className="w-full px-6 py-3">
              <div>
                <div className="mt-3 sm:mt-5">
                  <div className="inline-flex items-center w-full">
                    <h3 className="text-lg font-bold text-neutral-600 dark:text-white leading-6 lg:text-5xl">
                      {t("login.signIn")}
                    </h3>
                  </div>
                  <div className="mt-4 text-base text-gray-500 dark:text-gray-200">
                    <p>{t("login.title")}</p>
                  </div>
                </div>
              </div>
              <div className="mt-6 space-y-2">
                <Formik
                  validationSchema={validationSchema}
                  enableReinitialize
                  initialValues={{
                    email: "",
                    password: "",
                    error: "3",
                  }}
                  onSubmit={(values, { setErrors }) =>
                    userStore
                      .login(values)
                      .catch((error) => setErrors({ error: "Invalid Mail or Password" }))
                  }
                >
                  {({ handleSubmit, isValid, isSubmitting, dirty, errors }) => (
                    <Form className="relative " onSubmit={handleSubmit} autoComplete="off">
                      <MyTextInput placeholder={t("inputs.email")} name="email" />
                      <MyTextInput
                        placeholder={t("inputs.password")}
                        name="password"
                        type="password"
                      />

                      {errors.error != null ? (
                        <label className="absolute px-4 py-1 text-xs text-center text-white bg-red-600 rounded-lg dark:bg-red-700">
                          {errors.error}
                        </label>
                      ) : null}
                      <button
                        disabled={!isValid||!dirty||isSubmitting}
                        type="submit"
                        className="w-full mt-10 font-bold text-white disabled:bg-primary/25 disabled:dark:bg-primaryDark/25 bg-primary dark:bg-primaryDark hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 rounded-lg text-sm px-5 py-2.5 text-center"
                      >
                        {t("login.signIn")}
                      </button>
                      <p className="mt-2 text-sm font-light text-gray-500 dark:text-gray-100">
                        {t("login.signUpMsg")}{" "}
                        <NavLink to={"/register"} className="font-bold text-primary-600 hover:underline">
                          {t("login.signUp")}
                        </NavLink>
                      </p>
                    </Form>
                  )}
                </Formik>
              </div>
            </div>
            <div className="order-first hidden h-full lg:block">
              <img
                className="object-cover h-full bg-cover ltr:rounded-l-lg rtl:rounded-r-lg"
                src="https://picsum.photos/800?random=1"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default observer(SignIn);
