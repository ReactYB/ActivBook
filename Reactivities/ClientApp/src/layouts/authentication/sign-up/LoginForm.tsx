import { Form, Formik } from "formik";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../stores/store";
import { Dialog, Transition } from "@headlessui/react";
import { Fragment } from "react";
import MyTextInput from "../../../components/YBInputs/form/MyTextInput";
import { useTranslation } from "react-i18next";
import * as Yup from "yup";
import { NavLink } from "react-router-dom";
 

const LoginForm = () => {
  const { userStore, modalStore } = useStore();
  const {t} = useTranslation();

  const validationSchema = Yup.object({
    email: Yup.string()
      .required(t("validation.email").toString())
      .email(),
    password: Yup.string()
      .required( t("validation.password").toString())
  });

  return (
    <>
      <Transition appear show={modalStore.modal.open} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={modalStore.openModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>
          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex items-center justify-center h-full backdrop-blur">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full bg-white rounded-lg shadow dark:bg-gray-800 md:mt-0 sm:max-w-md xl:p-0">
                  <div className="p-6 sm:p-8">
                    <div className="py-5 mb-10 -mt-16 text-white rounded-lg bg-primary dark:bg-primaryDark">
                      <h1 className="text-xl font-bold leading-tight tracking-tight text-center text-white md:text-2xl ">
                        {t("login.title")}
                      </h1>
                    </div>
                    <Formik
                    validationSchema={validationSchema}
                      enableReinitialize
                      initialValues={{
                        email: "",
                        password: "",
                        error: "3",
                      }}
                      onSubmit={(values, { setErrors }) =>
                        userStore
                          .login(values)
                          .catch((error) => setErrors( error))
                      }
                    >
                      {({ handleSubmit, isValid, isSubmitting, dirty, errors }) => (
                        <Form className="relative " onSubmit={handleSubmit} autoComplete="off">
                          <MyTextInput placeholder={t("inputs.email")} name="email" />
                          <MyTextInput placeholder={t("inputs.password")} name="password" type="password" />

                          {errors.error != null ? (
                            <label className="absolute px-4 py-1 text-xs text-center text-white bg-red-600 rounded-lg dark:bg-red-700">
                              {errors.error}
                            </label>
                          ) : null}
                          <button
                            disabled={!isValid||!dirty||isSubmitting}
                            type="submit"
                            className=" w-full mt-10 font-bold text-white disabled:bg-primary/25 disabled:dark:bg-primaryDark/25 bg-primary dark:bg-primaryDark hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 rounded-lg text-sm px-5 py-2.5 text-center"
                          >
                            
                            {t("login.signIn")}
                          </button>
                          <p className="mt-2 text-sm font-light text-gray-500 dark:text-gray-100">
                            {t("login.signUpMsg")} {" "}
                            <NavLink to={"/register"} onClick={modalStore.closeModal}  className="font-bold text-primary-600 hover:underline">
                              {t("login.signUp")}
                            </NavLink>
                          </p>
                        </Form>
                      )}
                    </Formik>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

export default observer(LoginForm);
