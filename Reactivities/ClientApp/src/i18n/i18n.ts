// https://locize.com/blog/react-i18next/#prerequisites
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import en from './resources/en.json'
import ar from './resources/ar.json'

i18n
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    debug: true,
    fallbackLng: "en",
    keySeparator:".",
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    resources: {
      en,
      ar
    },
  });

export default i18n;
