import { createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../StoreReduxConfig";

interface darkModeState {
    theme: string
}

//Create a build in EntityAdapter qui va nous generer nos entity ainsi une initial state par default et avec des selectors predefini 
const darkModeAdapter = createEntityAdapter<string>()
export const darkmodeSlice = createSlice({
    name: "theme",
    initialState: darkModeAdapter.getInitialState<darkModeState>({ theme: 'system' }),
    reducers: {
        changeTheme: (state, action) => {
            if (action.payload === "dark") {
                localStorage.setItem("theme", "dark");
                document.documentElement.classList.add('dark');
                state.theme = action.payload
            } else if (action.payload === "light") {
                document.documentElement.classList.remove('dark');
                localStorage.setItem("theme", "light");
                state.theme = action.payload
            } else {
                if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
                    localStorage.setItem("theme", "dark");
                    document.documentElement.classList.add('dark');
                    state.theme = "dark"
                } else {
                    localStorage.setItem("theme", "light");
                    document.documentElement.classList.remove('dark');
                    state.theme = "light"
                }
            }
        },
    },
});

// Action creators are generated for each case reducer function
export const { changeTheme } = darkmodeSlice.actions;
// Export les selectors predefinie d'adapter
export const darkModeSelectors = darkModeAdapter.getSelectors((state: RootState) => state.darkMode)
