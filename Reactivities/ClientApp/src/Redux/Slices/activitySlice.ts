import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import agent from "../../api/agent";
import { Activity, ActivityParms } from "../../models/Activity";
import { RootState } from "../StoreReduxConfig";

interface ActivityState {
    activities:Activity[],
    activityLoaded: boolean,
    filtersLoaded: boolean,
    status: string,
    activityParams: ActivityParms,
}

//Create a build in EntityAdapter qui va nous generer nos entity ainsi une initial state par default et avec des selectors predefini 
const activityAdapter = createEntityAdapter<Activity>()

// Asyc function thunk pour fetch tout les donnees
export const fetchAllProductsAsync = createAsyncThunk<Activity[], void, { state: RootState }>(
    'Product/fetchAllsAsync',
    async (_, thunkAPI) => {
        try {
            // a definir
            const response = await agent.Activities.list()
            thunkAPI.dispatch(setMetaData(response))
            return response
        } catch (error: any) {
            return thunkAPI.rejectWithValue({ error: error })
        }
    }
)

// Asyc function thunk pour fetch une donnees
export const fetchProductAsync = createAsyncThunk<Activity, string>(
    'Product/fetchsOneAsync',
    async (id: string, thunkAPI) => {
        try {
            return await agent.Activities.details(id)
        } catch (error: any) {
            return thunkAPI.rejectWithValue({ error: error })
        }
    }
)
// Creer Slice avec initial state d'adapter
export const activitySlice = createSlice({
    name: 'Product',
    initialState: activityAdapter.getInitialState<ActivityState>({
        activityLoaded: false,
        filtersLoaded: false,
        status: "idle",
        activityParams: {
            orderBy: 'name',
            searchTerm: '',
            pageNumber: 1,
            pageSize: 9
        },
        activities: []
    }),
    reducers: {
        setProductParams: (state, action) => {
            state.activityLoaded = false
            // ajoutin page number to 1 pour reste la page lors d'un filtre
            state.activityParams = { ...state.activityParams, ...action.payload, pageNumber:1 }
        },
        setMetaData: (state, action) => {
            state.activities = action.payload
        },
        setPageNumber: (state, action) => {
            state.activityLoaded = false;
            state.activityParams = { ...state.activityParams, ...action.payload }
        },
        resetProductParams: (state) => {
            state.activityLoaded = false
            state.activityParams = {
                orderBy: 'name',
                searchTerm: '',
                pageNumber: 1,
                pageSize: 6
            }
        }
    },
    extraReducers: (builder => {
        //
        // Get All Data Reducers
        // 

        // 1 - Loading data
        builder.addCase(fetchAllProductsAsync.pending, (state) => {
            state.status = 'pendingFetchAllProduct';
        })
        // 2 - Stocker les donnees
        builder.addCase(fetchAllProductsAsync.fulfilled, (state, action) => {
            state.activityLoaded = true
            activityAdapter.setAll(state, action.payload)
            state.status = 'idle'
        })
        // 3 - En cas d'erreur
        builder.addCase(fetchAllProductsAsync.rejected, (state, action) => {
            state.activityLoaded = true
            state.status = 'idle';
        })

        //
        // Get One Data Reducers
        //

        // 1 - Loading
        builder.addCase(fetchProductAsync.pending, (state) => {
            state.status = 'pendingFetchProduct';
        })
        // 2 - Stocker les donnees
        builder.addCase(fetchProductAsync.fulfilled, (state, action) => {
            activityAdapter.upsertOne(state, action.payload)
            state.status = 'idle'
        })

        // 3 - En cas d'erreur
        builder.addCase(fetchProductAsync.rejected, (state, action) => {
            state.status = 'idle';
        })
    })
}
)


// Export les selectors predefinie d'adapter
export const { setProductParams, resetProductParams, setMetaData, setPageNumber } = activitySlice.actions;

// Export les selectors predefinie d'adapter
export const activitySelectors = activityAdapter.getSelectors((state: RootState) => state.activity)