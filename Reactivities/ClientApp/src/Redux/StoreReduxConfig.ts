import { configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { activitySlice } from "./Slices/activitySlice";
import { darkmodeSlice } from "./Slices/darkModeSlice";



// Invoking all Reducers
export const storeRedux = configureStore({
    reducer: {
        activity: activitySlice.reducer,
        darkMode: darkmodeSlice.reducer,
    }
})

// Getting type of state Type Script
export type RootState = ReturnType<typeof storeRedux.getState>

// Creating disptache with type
export const useTypeDispatch = () => useDispatch<typeof storeRedux.dispatch>()

// Creating disptache with type
export const useTypeSelector: TypedUseSelectorHook<RootState> = useSelector