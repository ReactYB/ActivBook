import axios, { AxiosResponse } from "axios";
import { toast } from "react-toastify";
import { historyIndex } from "..";
import { Activity } from "../models/Activity";
import { User, UserFormValues } from "../models/User";
import { store } from "../stores/store";

// Simulation d'une reponse lente
const sleep = (ms: number) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

axios.defaults.baseURL = "http://localhost:5000/api"

// Intersepteur de response
axios.interceptors.request.use(config => {
    const token = store.commonStore.token;
    if (token) config.headers!.Authorization = `Bearer ${token}`;
    return config;
})

// Intersepteur de response
axios.interceptors.response.use(async response => {
    await sleep(2000);
    return response;
}, (error: any) => {
    const { status, data, config } = error.response!;
    switch (status) {
        case 400:
            if (typeof data === "string") toast.error(data)
            if (config.method === 'get' && data.errors.hasOwnProperty('id')) historyIndex.push('/not-found')
            if (data.errors) {
                const modalStateErrors = [];
                for (const key in data.errors) {
                    if (data.errors[key]) {
                        modalStateErrors.push(data.errors[key])
                    }
                }
                throw modalStateErrors.flat()
            }
            break;
        case 401:
            toast.error('Unauthorized');
            break;
        case 404:
            toast.error('Not Found');
            historyIndex.push("not-found");
            break;
        case 500:
            store.commonStore.setServerError(data);
            historyIndex.push('/server-error')
            break;
    }
    return Promise.reject(error);
})

// Extraise Date de la Response
const responseBody = <T>(response: AxiosResponse<T>) => response.data;

// List des requests
const requests = {
    get: <T>(url: string) => axios.get<T>(url).then(responseBody),
    post: <T>(url: string, body: {}) => axios.post<T>(url, body).then(responseBody),
    put: <T>(url: string, body: {}) => axios.put<T>(url, body).then(responseBody),
    delete: <T>(url: string) => axios.delete<T>(url).then(responseBody),
}

// Activity calls
const Activities = {
    list: () => requests.get<Activity[]>('/activities'),
    details: (id: string) => requests.get<Activity>(`/activities/${id}`),
    create: (activity: Activity) => requests.post<void>('/activities', activity),
    update: (activity: Activity) => requests.put<void>(`/activities/${activity.id}`, activity),
    delete: (id: string) => requests.delete<void>(`/activities/${id}`)
}
// Activity calls
const Account = {
    current: () => requests.get<User>('/account/account'),
    login: (user: UserFormValues) => requests.post<void>('/account/login', user),
    register: (user: UserFormValues) => requests.post<void>('/account/register', user),
}
const Agent = { Activities, Account }

export default Agent;