import { Route, Switch, useLocation } from "react-router-dom";
import { Theme, ToastContainer } from "react-toastify";
import { useEffect } from "react";
import { useStore } from "./stores/store";
import Header from "./layouts/navigation/header/Header";
import LoginForm from "./layouts/authentication/sign-up/LoginForm";
import NotFound from "./layouts/pages/Errors/NotFound";
import ServerError from "./layouts/pages/Errors/ServerError";
import TestError from "./layouts/pages/Errors/TestError";
import ActivityDashboard from "./layouts/activities/activityDashboard/ActivityDashboard";
import ActivityForm from "./layouts/activities/activityForm/ActivityForm";
import YBDrawer from "./components/YBDrawer/YBDrawer";
import Profil from "./layouts/pages/account/Profil";
import Footer from "./layouts/navigation/footer/Footer";
import SignIn from "./layouts/authentication/sign-in/SignIn";
import CGU from "./layouts/about/CGU";
import PlanSite from "./layouts/about/PlanSite";
import YBScroll from "./components/YBScroll/YBScroll";
import Policy from "./layouts/about/Policy";
import LoginPage from "./layouts/authentication/sign-up/LoginPage";
import { useTypeDispatch, useTypeSelector } from "./Redux/StoreReduxConfig";
import { changeTheme } from "./Redux/Slices/darkModeSlice";
import { localStorageUtil } from "./utils/utils";

const App = () => {
  const { key } = useLocation();
  const { commonStore, userStore, translationStore } = useStore();
  const {theme} = useTypeSelector((state) => state.darkMode);
  const dispatch = useTypeDispatch();
  const A = localStorageUtil.get("theme",'system')
  useEffect(() => {
    dispatch(changeTheme(A));
    if (commonStore.token) {
      userStore.getUser().finally(() => commonStore.setAppLoaded());
    } else commonStore.setAppLoaded();
    
  }, [commonStore, userStore, theme]);

  return (
    <div className="min-h-screen transition-colors duration-500 ease-in-out font-Roboto rtl:font-Mada bg-gradient-to-r from-primaryDark/20 to to-primary/20 dark:bg-gray-800">
      <ToastContainer
        className="top-16"
        rtl={translationStore.rtl}
        position="top-right"
        theme={theme === "dark" ? "dark" : "light"}
      />
      <YBDrawer />
      <Header />
      <LoginForm />
      <YBScroll>
        <Route exact path="/" component={ActivityDashboard} />
        <Route
          path={"/(.+)"}
          render={() => (
            <Switch>
              <Route
                exact
                key={key}
                path={["/createActivity", "/manage/:id"]}
                component={ActivityForm}
              />
              <Route path={"/profil"} component={Profil} />
              <Route path={"/errors"} component={TestError} />
              <Route path={"/login"} component={LoginPage} />
              <Route path={"/server-error"} component={ServerError} />
              <Route path={"/register"} component={SignIn} />
              <Route path={"/about/CGU"} component={CGU} />
              <Route path={"/about/PlanSite"} component={PlanSite} />
              <Route path={"/about/Policy"} component={Policy} />
              <Route component={NotFound} />
            </Switch>
          )}
        ></Route>
      </YBScroll>
      <Footer />
    </div>
  );
};
export default App;
