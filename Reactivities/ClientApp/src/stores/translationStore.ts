import { makeAutoObservable, runInAction } from "mobx"
import i18n from "../i18n/i18n";
export default class translationStore {
    selectedLang = "en";
    appLang: string[] = ["en", "ar"];
    rtl = false

    constructor() {
        makeAutoObservable(this);
        if (!localStorage.getItem("lang")) {
            localStorage.setItem("lang", this.selectedLang); i18n.changeLanguage(this.selectedLang); if (this.selectedLang === "ar") {
                document.documentElement.setAttribute('dir', 'rtl');
                this.rtl = true
            } else {
                document.documentElement.setAttribute('dir', 'ltr'); this.rtl = false
            }
        }
        else {
            runInAction(() => this.selectedLang = localStorage.getItem("lang")!); i18n.changeLanguage(this.selectedLang); if (this.selectedLang === "ar") {
                document.documentElement.setAttribute('dir', 'rtl'); this.rtl = true
            } else {
                document.documentElement.setAttribute('dir', 'ltr'); this.rtl = false
            } }
    }

    setLang = (value: string) => {
        this.selectedLang = value;
        localStorage.setItem("lang", value);
        i18n.changeLanguage(value)
        if (this.selectedLang === "ar") {
            document.documentElement.setAttribute('dir', 'rtl'); this.rtl = true
        } else {
            document.documentElement.setAttribute('dir', 'ltr'); this.rtl = false
        }
    };
}