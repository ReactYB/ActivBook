import { format } from "date-fns";
import { makeAutoObservable, runInAction } from "mobx";
import Agent from "../api/agent";
import { Activity } from "../models/Activity";

export default class ActivityStore {
    activityRegistry = new Map<string, Activity>();
    selectedActivity: Activity | undefined = undefined;
    editMode = false;
    loading = true;
    loadingInitial = true;
    // Constructeur
    constructor() {
        makeAutoObservable(this)
    }
    // Activities
    get activities() {
        return Array.from( this.activityRegistry.values() );

    }
    // Activities  By Date
    get activitiesByDate() {
        return Array.from(this.activityRegistry.values()).sort((a, b) => a.date!.getTime() - b.date!.getTime());
    }
    // Group By Date
    // Need to be EXPLAINED Brain POWER Needeeedddddd
    get groupedActivities() {
        return Object.entries(
            this.activitiesByDate.reduce((activities, activity) => {
                const date = format(activity.date!, 'dd MMM yyyy');
                activities[date] = activities[date] ? [...activities[date], activity] : [activity];
                return activities;
            }, {} as { [key: string]: Activity[] }))
    }
    // Action changing LoadingInitial state
    setLoadingInitial = (state: boolean) => {
        this.loadingInitial = state;
    }
    // Action changing loading state
    setLoading = (state: boolean) => {
        this.loading = state;
    }
    // Action changing editMode state
    setEditMode = (state: boolean) => {
        this.editMode = state;
    }
    // Get Activities
    loadActivities = async () => {
        // this.setLoadingInitial(true);
        try {
            const responseList = await Agent.Activities.list();
            responseList.forEach((activity) => {
                this.setActivity(activity);
            });
            this.setLoadingInitial(false);
        } catch (error) {
            this.setLoadingInitial(false);
        }
    }
    // Get Activity
    loadActivity = async (id: string) => {
        let activity = this.getActivity(id);
        if (activity) {
            this.selectedActivity = activity;
            return activity;
        } else {
            this.setLoadingInitial(true);
            try {
                activity = await Agent.Activities.details(id);
                this.setActivity(activity);
                runInAction(() => {
                    this.selectedActivity = activity
                    this.setLoadingInitial(false);
                })
                return activity;
            } catch (error) {
                console.log(error);
                this.setLoadingInitial(false);
            }
        }
    }
    // Private Helper
    private setActivity = (activity: Activity) => {
        activity.date = new Date(activity.date!);
        this.activityRegistry.set(activity.id, activity);
    }
    private getActivity = (id: string) => {
        return this.activityRegistry.get(id);
    }
    // CRUD
    // Create activity
    createActivity = async (activity: Activity) => {
        this.setLoading(true);
        try {
            await Agent.Activities.create(activity);
            runInAction(() => {
                this.activityRegistry.set(activity.id, activity);
                this.setEditMode(false);
                this.setLoading(false);
            })
        } catch (error) {
            runInAction(() => {
                this.setLoading(false);
            })
            console.log(error);
        }
    };
    // Update activity
    updateActivity = async (activity: Activity) => {
        this.setLoading(true);
        try {
            await Agent.Activities.update(activity);
            runInAction(() => {
                this.activityRegistry.set(activity.id, activity);
                this.selectedActivity = activity;
                this.setEditMode(false);
                this.setLoading(false);
            })
        } catch (error) {
            runInAction(() => {
                this.setLoading(false);
            })
            console.log(error);
        }
    };
    // Delete activity
    deleteActivity = async (id: string) => {
        this.setLoading(true);
        try {
            await Agent.Activities.delete(id);
            runInAction(() => {
                this.activityRegistry.delete(id);
                this.setLoading(false);
            })
        } catch (error) {
            runInAction(() => {
                this.setLoading(false);
            })
            console.log(error);
        }
    }
    openEditMode = () => {
        this.editMode = true;
    }
}