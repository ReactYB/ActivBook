import { makeAutoObservable, runInAction } from "mobx"
import { historyIndex } from "..";
import Agent from "../api/agent";
import { User, UserFormValues } from "../models/User";
import { store } from "./store";

export default class userStore {
    user: User | null = null;
    constructor() {
        makeAutoObservable(this)
    }
    get isLoggedIn() {
        return !!this.user;
    }
    login = async (creds: UserFormValues) => {
        try {
            const user: any = await Agent.Account.login(creds);
            store.commonStore.setToken(user.token);
            runInAction(() => this.user = user);
            historyIndex.push("/");
            store.modalStore.closeModal()
        }
        catch (error) { throw error }
    }
    register = async (creds: UserFormValues) => {
        try {
            const user: any = await Agent.Account.register(creds);
            store.commonStore.setToken(user.token);
            runInAction(() => this.user = user);
            historyIndex.push("/Profil");
        }
        catch (error) { throw error }
    }
    logout = () => {
        store.commonStore.setToken(null);
        window.localStorage.removeItem('jwt');
        this.user = null;
        historyIndex.push("/login")
    }
    getUser = async () => {
        try {
            const user = await Agent.Account.current();
            console.log(user);
            
            runInAction(() => this.user = user)
        }
        catch (error) {
            console.log(error);
        }
    }
}