import { makeAutoObservable } from "mobx"

interface Modal{
    open : boolean;
}

export default class ModalStore {
    modal:Modal = {
        open: false,
    }
    constructor(){
        makeAutoObservable(this)
    }
    openModal = () => {
        this.modal.open = !this.modal.open;        
    }
    closeModal = () => {
        this.modal.open = false;
        
    }
}