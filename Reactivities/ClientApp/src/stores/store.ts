import { createContext, useContext } from "react";
import ActivityStore from "./activityStore";
import CommonStore from "./commonStore";
import ModalStore from "./modalStore";
 
import UserStore from "./userStore";
import TranslationStore from "./translationStore";
// Interface Des Stores
interface Store {
    activityStore: ActivityStore;
    commonStore: CommonStore;
    userStore: UserStore;
    modalStore: ModalStore;
 
    translationStore: TranslationStore;
}
// Instancier les Stores
export const store: Store = {
    activityStore: new ActivityStore(),
    commonStore: new CommonStore(),
    userStore: new UserStore(),
    modalStore: new ModalStore(),
 
    translationStore: new TranslationStore(),
}
// Créer le Context
export const StoreContext = createContext(store);

// Utiliser le Context 
export function useStore() {
    return useContext(StoreContext);
}