import { makeAutoObservable, reaction } from "mobx"
import { ServerError } from "../models/ServerError";

export default class CommonStore {
    error: ServerError | null = null;
    token: string | null = window.localStorage.getItem('jwt');
    appLoaded = false;
    colorTheme: string[] = ["charcoal", "primary", "maize", "sandy","primaryDark"];
    currentTheme: string = this.colorTheme[0] ;
    miniSidenav = false;
    constructor(){
        makeAutoObservable(this);
        // Reacting to Jwt, change only when modified, dosent run first tick
        reaction(()=>this.token,token=>{
            if(token){window.localStorage.setItem('jwt',token)}
            else window.localStorage.removeItem('jwt')
        })
    }
    setServerError = (error:ServerError) =>{
        this.error = error;
    }
    setToken = (token:string |null) => {
        this.token = token;
    }
    setAppLoaded = () => {
        this.appLoaded = true
    }
    setColorTheme = (id:number) => {
        if(id>=0 && id<this.colorTheme.length) this.currentTheme = this.colorTheme[id]
        else this.currentTheme = this.colorTheme[0]
    }
    setMiniSidenav = () => {
        this.miniSidenav = !this.miniSidenav;
    };
}