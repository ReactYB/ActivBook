import { useField } from "formik";
import DatePicker, { ReactDatePickerProps } from "react-datepicker";
import { useTranslation } from "react-i18next";

const MyDateInput = (props: Partial<ReactDatePickerProps>) => {
  const {t} = useTranslation();
  const [field, meta, helpers] = useField(props.name!);
  return (
    <>
      <div className="relative mb-3">
        <label
          htmlFor={props.name}
          className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
        >
         {t("inputs.date").charAt(0).toUpperCase() + t("inputs.date").slice(1)}
        </label>
        <DatePicker
          {...props}
          {...field}
          isClearable={true}
          placeholderText="I have been cleared!"
          onChange={(value) => helpers.setValue(value)}
          selected={field.value && (new Date(field.value) || null)}
          className={`${
            meta.touched && meta.error
              ? "bg-red-50 dark:bg-red-900/10 dark:placeholder-gray-400 "
              : "bg-gray-50 dark:bg-slate-700 dark:placeholder-gray-200"
          }  block w-full h-12 px-4  rtl:-space-x-3 ltr:space-x-3 rtl:text-right ltr:text-left rounded-lg shadow-sm sm:flex focus:outline-none ring-2 ring-gray-50 dark:ring-slate-700 focus:ring-slate-200 hover:bg-slate-200 dark:hover:bg-slate-600  dark:text-slate-300 `}
          />
        {meta.touched && meta.error ? (
          <label className="absolute ltr:right-0 rtl:left-0 px-4 py-1 mt-1 text-xs text-center text-white bg-red-600 rounded-lg dark:bg-red-700">
            {meta.error}
          </label>
        ) : null}
      </div>
    </>
  );
};

export default MyDateInput;