import { useEffect } from 'react';
import { useLocation, withRouter } from 'react-router-dom';

const YBScroll = ({ history, children  }:any) => {
    const { pathname } = useLocation();
    useEffect(() => {
        const unlisten = history.listen(() => {
          window.scrollTo(0, 0);
        });
        return () => {
          unlisten();
        }
      }, [pathname,history]);
    
      return <>{children}</>;
}

export default withRouter(YBScroll)