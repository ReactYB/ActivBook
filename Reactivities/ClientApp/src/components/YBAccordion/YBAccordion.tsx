import { Disclosure, Transition } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/20/solid";

const YBAccordion = () => {
  return (
    <div className="w-full px-4 pb-16">
      <div className="max-w-5xl mx-auto ">
        <Disclosure defaultOpen>
          {({ open }) => (
            <>
              <Disclosure.Button
                className={`${
                  open ? "" : " rounded-b-lg"
                } flex mt-2 w-full transition-all duration-150 delay-150 shadow-sm text-base font-medium text-white bg-primary hover:bg-primary/75 dark:bg-slate-700 dark:hover:bg-bg-slate-700/75 py-2 px-4 justify-between rounded-t-lg  focus:outline-none `}
              >
                <span>What is your refund policy?</span>
                <ChevronUpIcon
                  className={`${
                    open ? "rotate-180 transform" : ""
                  } h-6 w-6 text-white transition-transform delay-150 duration-150`}
                />
              </Disclosure.Button>
              <Transition
                show={open}
                enter="transition-all duration-200"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="transition-all duration-350"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Disclosure.Panel className="text-gray-700 font-semibold dark:text-gray-400 bg-gray-300 dark:bg-gray-800 px-6 py-8 rounded-b-lg">
                  If you're unhappy with your purchase for any reason, email us within 90 days and
                  we'll refund you in full, no questions asked.
                </Disclosure.Panel>
              </Transition>
            </>
          )}
        </Disclosure>
        <Disclosure>
          {({ open }) => (
            <>
              <Disclosure.Button
                className={`${
                  open ? "" : " rounded-b-lg"
                } flex mt-2 w-full transition-all duration-150 delay-150 shadow-sm text-base font-medium text-white bg-primary hover:bg-primary/75 dark:bg-slate-700 dark:hover:bg-bg-slate-700/75 py-2 px-4 justify-between rounded-t-lg  focus:outline-none `}
              >
                <span>What is your refund policy?</span>
                <ChevronUpIcon
                  className={`${
                    open ? "rotate-180 transform" : ""
                  } h-6 w-6 text-white transition-transform delay-150 duration-150`}
                />
              </Disclosure.Button>
              <Transition
                show={open}
                enter="transition-all duration-200"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="transition-all duration-350"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Disclosure.Panel className="text-gray-700 font-semibold dark:text-gray-400 bg-gray-300 dark:bg-gray-800 px-6 py-8 rounded-b-lg">
                  If you're unhappy with your purchase for any reason, email us within 90 days and
                  we'll refund you in full, no questions asked.
                </Disclosure.Panel>
              </Transition>
            </>
          )}
        </Disclosure>
        <Disclosure>
          {({ open }) => (
            <>
              <Disclosure.Button
                className={`${
                  open ? "" : " rounded-b-lg"
                } flex mt-2 w-full transition-all duration-150 delay-150 shadow-sm text-base font-medium text-white bg-primary hover:bg-primary/75 dark:bg-slate-700 dark:hover:bg-bg-slate-700/75 py-2 px-4 justify-between rounded-t-lg focus:outline-none`}
              >
                <span>What is your refund policy?</span>
                <ChevronUpIcon
                  className={`${
                    open ? "rotate-180 transform" : ""
                  } h-6 w-6 text-white transition-transform delay-150 duration-150`}
                />
              </Disclosure.Button>
              <Transition
                show={open}
                enter="transition-all duration-200"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="transition-all duration-350"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Disclosure.Panel className="text-gray-700 font-semibold dark:text-gray-400 bg-gray-300 dark:bg-gray-800 px-6 py-8 rounded-b-lg">
                  If you're unhappy with your purchase for any reason, email us within 90 days and
                  we'll refund you in full, no questions asked.
                </Disclosure.Panel>
              </Transition>
            </>
          )}
        </Disclosure>
      </div>
    </div>
  );
};

export default YBAccordion;
