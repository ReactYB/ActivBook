import { Switch } from "@headlessui/react";
import { useTypeDispatch, useTypeSelector } from "../../Redux/StoreReduxConfig";
import { changeTheme } from "../../Redux/Slices/darkModeSlice";

interface Props {
  className: string;
}

const YBToggleDark = (props: Props) => {
  const {theme} = useTypeSelector(state => state.darkMode)
  const dispatch = useTypeDispatch();
  return (
    <>
      <Switch
        onChange={() =>
          theme === "light"
            ? dispatch(changeTheme("dark"))
            : dispatch(changeTheme("light"))
        }
        className={`${theme === "dark" ? "bg-slate-600" : "bg-blue-400"}
          relative inline-flex h-[24px] w-[48px] shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out   focus:outline-none focus-visible:ring-2  focus-visible:ring-white focus-visible:ring-opacity-75 ${
            props.className
          }`}
      >
        <span className="sr-only">change theme</span>
        <span
          aria-hidden="true"
          className={`${
            theme === "dark"
              ? "rtl:-translate-x-6 ltr:translate-x-6 bg-orange-200"
              : "translate-x-0 bg-yellow-500"
          }
             relative  z-10 pointer-events-none inline-block h-[20px] w-[20px] transform rounded-full shadow ring-0 transition duration-700 ease-[cubic-bezier(0.445, 0.05, 0.55, 0.95)] `}
        >
          <span
            className={`${
              theme === "dark"
                ? "opacity-1 h-[6px] w-[6px] bg-slate-400 rtl:left-[10%] ltr:right-[10%]"
                : "h-[2px] w-[15px] bg-gray-50 shadow rtl:left-[-20%] ltr:right-[-20%] z-5"
            } absolute  inline-block transition-all duration-500 delay-200 ease-[cubic-bezier(0.445, 0.05, 0.55, 0.95)] rounded-[100%]  top-[15%] `}
          ></span>
          <span
            className={`${
              theme === "dark"
                ? "opacity-1 w-[3px] bg-slate-400 top-[25%] rtl:right-[20%] ltr:left-[20%] h-[3px]"
                : "w-[16px] rtl:right-[65%] ltr:left-[65%] bg-gray-50 shadow top-[40%]  h-[2px]"
            } absolute inline-block transition-all duration-500 delay-200 ease-[cubic-bezier(0.445, 0.05, 0.55, 0.95)] rounded-[100%]`}
          ></span>
          <span
            className={`${
              theme === "dark"
                ? "opacity-1 w-1.5  h-1.5 bg-slate-400 rtl:right-[40%] ltr:left-[40%]"
                : "h-[2px] w-[18px] bg-gray-50 rtl:right-[35%] ltr:left-[35%] shadow -z-10"
            } absolute inline-block transition-all duration-500 delay-200 ease-[cubic-bezier(0.445, 0.05, 0.55, 0.95)] rounded-[100%]  top-[65%]`}
          ></span>
        </span>
      </Switch>
    </>
  );
};

export default YBToggleDark;
