import { Fragment } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon,  GlobeAltIcon } from "@heroicons/react/20/solid";
import { useStore } from "../../stores/store";
interface Props {
  className: string;
}
const YBToggleLang = (props: Props) => {
  const { translationStore } = useStore();

  return (
    <div className={`uppercase ${props.className}`}>
      <Listbox value={translationStore.selectedLang} onChange={translationStore.setLang}>
        <div className="relative">
          <Listbox.Button className="flex hover:bg-primary/10 text-primary dark:text-primaryDark gap-1 relative w-full rounded-lg cursor-pointer text-sm px-2 py-2.5 text-center mr-3 md:mr-0 focus:outline-none">
            <span className="block truncate uppercase">{translationStore.selectedLang}</span>
            <span className="pointer-events-none flex items-center">
              <GlobeAltIcon className="h-5 w-5" aria-hidden="true" />
            </span>
          </Listbox.Button>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className="absolute mt-1  overflow-auto rounded-md bg-white dark:bg-slate-800  text-base shadow-lg sm:text-sm">
              {translationStore.appLang.map((lang, personIdx) => (
                <Listbox.Option
                  key={personIdx}
                  className={({ active }) =>
                    `flex px-2 text-center cursor-pointer select-none py-1 text-gray-900 dark:text-white ${
                      active ? "bg-primary/10 font-semibold" : ""
                    }`
                  }
                  value={lang}
                >
                  {({ selected }) => (
                    <>
                      <span
                        className={`block truncate ${selected ? "font-medium" : "font-normal"}`}
                      >
                        {lang}
                      </span>
                      {selected ? (
                        <span className=" flex items-center pl-1 text-primary dark:text-amber-600">
                          <CheckIcon className="h-5 w-5" aria-hidden="true" />
                        </span>
                      ) : null}
                    </>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  );
};

export default YBToggleLang;
