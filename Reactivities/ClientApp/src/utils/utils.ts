import { ClassValue, clsx } from "clsx"
import { twMerge } from "tailwind-merge"

export function cn(...inputs: ClassValue[]) {
    return twMerge(clsx(inputs))
}
export const localStorageUtil = {
    set: (key: string, value: any) => {
        localStorage.setItem(key, JSON.stringify(value));
    },
    get: <T>(key: string, defaultValue?: T): T => {
        const value = localStorage.getItem(key);
        return (value ? String(value) : String(defaultValue)) as T;
    },
    remove: (key: string) => {
        localStorage.removeItem(key);
    },
};