import { useEffect, useState } from "react";

const useHover = () => {
  useEffect(() => {
    const cursorEl = document.createElement("div") as HTMLElement;
    cursorEl.classList.add("cursor");
    cursorEl.setAttribute(
      "style",
      "pointer-events: none;position: fixed;padding: 0.3rem;background-color: rgb(59, 37, 136);border-radius: 50%;mix-blend-mode: difference;transition: transform 0.3s ease;"
    );
    document.querySelector('#root')!.appendChild(cursorEl);
    return () => {
      document.querySelector("#root")!.removeChild(cursorEl);
      document.body.style.cursor = "auto";
    };
  }, []);

  // MouseData State
  //
  const [MouseStats, setMouseStats] = useState({
    clientX: 0,
    clientY: 0,
    offsetX: 0,
    offsetY: 0,
    clientWidth: 0,
    clientHeight: 0,
    type: "",
  });

  // In Out Mouse State
  //
  const [activeElement, setActiveElement] = useState(false);

  // Handle Mouse Move
  //
  const handleMouseMove = (e: any) => {
    e.persist();
    setMouseStats({
      ...MouseStats,
      clientX: e.clientX,
      clientY: e.clientY,
      offsetX: e.nativeEvent.offsetX,
      offsetY: e.nativeEvent.offsetY,
      clientWidth: e.currentTarget.clientWidth,
      clientHeight: e.currentTarget.clientHeight,
      type: e.type,
    });
  };

  // Handle Mouse Enter
  //
  const handleMouseOn = (e: any) => {
    e.persist();
    setActiveElement(true);
    const cursor = document.querySelector(".cursor") as HTMLElement;
    document.body.style.cursor = "none";
    cursor.style.transform = "scale(15)";
    cursor.style.transition = "transform 0.4s ";
  };

  // Handle Mouse Leave
  //
  const handleMouseLeave = (e: any) => {
    e.persist();
    document.body.style.cursor = "auto";
    const cursor = document.querySelector(".cursor") as HTMLElement;
    cursor.style.transform = "scale(.01)";
    setActiveElement(false);
  };

  const elementTransform = (element: HTMLElement) => {
    const cursor = document.querySelector(".cursor") as HTMLElement;
    const move = 25;
    const xMove =
      (MouseStats.clientX / MouseStats.clientWidth) * (move * 0.5) - move;
    const yMove =
      (MouseStats.clientY / MouseStats.clientHeight) * (move * 0.5) - move;

    if (activeElement) {
      element.style.transform = `translate(${xMove}px, ${yMove}px)`;
      cursor.style.left = `${MouseStats.clientX}px`;
      cursor.style.top = `${MouseStats.clientY}px`;
    }
  };

  const logMouse = () => {
    console.log({ ...MouseStats, activeElement });
  };

  return {
    handleMouseMove,
    logMouse,
    elementTransform,
    handleMouseOn,
    handleMouseLeave,
  };
};

export default useHover;
