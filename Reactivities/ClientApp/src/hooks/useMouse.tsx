import {  useState } from "react";

const UseMouse = () => {
  const [state, setState] = useState({ x: 0, y: 0 });
  const maxShadowDistance = 10;
  const maxShadowBlur = 12;

  //get rondom x,y values
  //
  const getRandom = (element:HTMLElement) => {
    const style = getComputedStyle(element);
    const pageWidth = style.getPropertyValue("width");
    const pageHeight = style.getPropertyValue("height");
    const x = (state.x / parseInt(pageWidth)) * 2 - 1;
    const y = (state.y / parseInt(pageHeight)) * 2 - 1;
    const distance = Math.max(Math.abs(x), Math.abs(y));
    return { x, y, distance };
  };

  // Gradiant Change function
  //
  const setGradient = (
    element: HTMLElement
  ) => {
    const { x, y, distance } = getRandom(element);
    const lightAngleRadians = Math.atan2(y, x);
    const lightAngle = lightAngleRadians * (180 / Math.PI) - 150;
    const lightDistance = 10 * distance + 90;
    element.style.background = `linear-gradient(${lightAngle}deg, hsl(243, 100%, 7%, 1) 0%, hsl(190, 100%, 30%, 1) ${lightDistance}%)`;
  };

  // Shadow Change function
  //
  function setShadow(
    element: HTMLElement
  ) {
    const { x, y, distance } = getRandom(element);
    const xShadow = -x * maxShadowDistance;
    const yShadow = -y * maxShadowDistance;
    const shadowBlur = distance * maxShadowBlur + 1;
    const shadowOpacity = (1 - distance) * 0.5 + 0.2;
    element.style.textShadow = `${xShadow}px ${yShadow}px ${shadowBlur}px hsla(120,100%,25%, ${shadowOpacity})`;
  }

  // Mouse Move function
  //
  const handleMouseMove = (e: any) => {
    e.persist();
    setState((mouse) => ({ ...mouse, x: e.clientX, y: e.clientY }));
  };

  // Just a Logger function
  //
  const logMouse = () => {
    console.log(state.x, state.y);
  };

  // Print Mouse location a gogogogogo
  //
  const printMouse = () => {
    return { x: state.x, y: state.y };
  };
  return {
    handleMouseMove,
    setShadow,
    setGradient,
    logMouse,
    printMouse,
  };
};
export default UseMouse;