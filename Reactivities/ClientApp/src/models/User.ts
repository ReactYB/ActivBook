export interface User {
    userName: string;
    displayName: string;
    token: string;
    image?: string;
    email: string;
    userId: string
}
export interface UserFormValues {
    email: string;
    displayname?: string;
    password: string;
    image?: string
}
