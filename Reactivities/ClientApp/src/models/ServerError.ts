export interface ServerError {
    statutCode: number,
    message: string
    details: string
}