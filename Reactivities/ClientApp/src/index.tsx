import React from "react";
import ReactDOM from "react-dom/client";
import "react-toastify/dist/ReactToastify.min.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./assets/css/tailwind.css";
import 'react-calendar/dist/Calendar.css';
import 'react-datepicker/dist/react-datepicker.css'
import {  Router } from "react-router-dom";
import {createBrowserHistory} from 'history'
import { store, StoreContext } from "./stores/store";
import App from "./App";
import { Provider } from "react-redux";
import { storeRedux } from "./Redux/StoreReduxConfig";

export const historyIndex = createBrowserHistory();

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <Router history={historyIndex}>
    <React.StrictMode>
       <Provider store={storeRedux}>
      <StoreContext.Provider value={store}>
        <App />
      </StoreContext.Provider></Provider>
    </React.StrictMode>
  </Router>
);
