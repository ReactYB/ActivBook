/** @type {import('tailwindcss').Config} */
export default {
  darkMode: "class",
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        AmiriQuran: ["AmiriQuran"],
        Mada: ["Mada"],
        Roboto: ["Roboto"],
      },
      // https://coolors.co/264653-2a9d8f-e9c46a-f4a261-e76f51
      colors: {
        charcoal: "#264653",
        primary: "#2a9d8f",
        maize: "#e9c46a",
        sandy: "#f4a261",
        primaryDark: "#e76f51 ",
      },
    },
  },
  plugins: [],
  variants: {},
};
